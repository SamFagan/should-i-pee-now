axios.get('https://p58oqmneac.execute-api.us-east-1.amazonaws.com/prod/message')
    .then(function (response) {
        for (const key in response.data.body) {
            setVacency(response.data.body[key]);
        }
    })

function setVacency(toilet) {
    var matches = document.querySelectorAll(".wall");
    for (value in matches) {
        if (!matches[value].querySelector(".toilet")){
            var div = document.createElement('div');
            div.className = "toilet";


            div.appendChild(createDoorImage(toilet.status == 1))
            div.appendChild(this.assignId(toilet.sensor_id));
            matches[value].appendChild(div);
            break;
        }
    }
}

function createDoorImage(enableDoorOpen) {
    var img = document.createElement("IMG");
    if (enableDoorOpen) {
        img.src = "./images/DoorOpen.png";
    } else {
        img.src = "./images/DoorClosed.png";
    }
    
    return img;
}

function assignId(value) {
    var img = document.createElement("IMG");
    img.className = "doorName";
    switch (value) {
        case 0: {
            img.src = "./images/VIP.png"
            return img;
        }
        case 1: {
            img.src = "./images/pleb1.png"
            return img;
        }
        case 2: {
            img.src = "./images/pleb2.png"
            return img;
        }
        case 3: {
            img.src = "./images/pleb3.png"
            return img;
        }
    }
}
