import json
import boto3
import uuid


def lambda_handler(event, context):    
    request_body = event
    s3_client = boto3.client('s3')

    request_body_json = json.dumps(request_body)

    sensor_id = request_body["id"]
    timestamp = request_body["start_time"]
    
    s3_client.put_object(Key=f"{sensor_id}/frequency/latest.json", Bucket="hack-toilet-store", Body=request_body_json)
    s3_client.put_object(Key=f"{sensor_id}/frequency/{timestamp}.json", Bucket="hack-toilet-store", Body=request_body_json)
    
    return {
        'statusCode': 200,
        'body': request_body
    }
