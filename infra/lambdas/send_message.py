import json
import boto3
import uuid


def lambda_handler(event, context):    
    request_body = event
    
    s3_client = boto3.client('s3')
    
    message_body = {
        "message_id": str(uuid.uuid4()),
        "sensor_id": request_body["id"],
        "status": request_body["status"],
        "timestamp": request_body["timestamp"],
    }

    message_body_json = json.dumps(message_body)

    sensor_id = request_body["id"]
    timestamp = request_body["timestamp"]
    
    s3_client.put_object(Key=f"{sensor_id}/latest.json", Bucket="hack-toilet-store", Body=message_body_json)
    s3_client.put_object(Key=f"{sensor_id}/{timestamp}.json", Bucket="hack-toilet-store", Body=message_body_json)
    
    return {
        'statusCode': 200,
        'body': message_body
    }
