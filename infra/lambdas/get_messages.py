import json
import boto3
import uuid


def lambda_handler(event, context):
    s3 = boto3.resource('s3')
    
    response_dict = {}

    for sensor_id in [0,1,2,3]:
        try:
            obj = s3.Object("hack-toilet-store", f"{sensor_id}/latest.json")
            json_blob = json.loads(obj.get()['Body'].read().decode('utf-8'))
        except:
            json_blob = {}
        response_dict[sensor_id] = json_blob
            
    
    return {
        'statusCode': 200,
        'body': response_dict
    }
